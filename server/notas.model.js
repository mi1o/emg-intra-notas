
const mongoose = require('bluebird').promisifyAll(require('mongoose'));
const { Schema } = require('mongoose');

const NotasSchema = new Schema({
  rut: {
    type: String,
    required: true,
  },
  nombres: String,
  apaterno: String,
  amaterno: String,
  autorizacion: String,
  anio: Number,
  periodo: Number,
  nivel: String,

  lenguaje: Number,
  matematicas: Number,
  sociales: Number,
  cienciasNaturales: Number,
  idiomaExtranjero: Number,
  laboral: Number,
  promedio: Number,
  situacionFinal: String,
  fechaExamen: String
});

module.exports = mongoose.model('Notas', NotasSchema);
