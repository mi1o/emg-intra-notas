
const mongoose = require('bluebird').promisifyAll(require('mongoose'));
const { Schema } = require('mongoose');

const UsuarioSchema = new Schema({
  rut: {
    type: String,
    unique: true,
    required: true,
  },
  nombres: String,
  apaterno: String,
  amaterno: String,
  funcion: String,
  asignatura: String,
});

module.exports = mongoose.model('Usuarios', UsuarioSchema);
