
const Usuario = require('./usuarios.model');
const { respondWithResult,
        saveUpdates,
        removeEntity,
        handleEntityNotFound,
        handleError } = require('./helpers');

const _ = require('lodash/merge')

module.exports.lista = (req, res) => Usuario.findAsync()
  .then(respondWithResult(res))
  .catch(handleError(res));

module.exports.crear = (req, res) => {
  Usuario.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
};

module.exports.borrar = (req, res) => Usuario.findOneAsync({ rut: req.params.rut })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));

module.exports.editar = (req, res) => {
  Usuario.findAsync({ rut: req.body.rut })
    .then(handleEntityNotFound(res))
    .then(usuario => {
      usuario = _.merge(usuario, req.body);
      usuario.saveAsync();
    })
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
};

module.exports.seed = (req, res) => {
  Usuario.createAsync([{
    rut: '16.767.929-3',
    nombres: 'Camilo Nicolás',
    apaterno: 'Placencia',
    amaterno: 'Ulloa',
    funcion: 'Coordinador',
    asignatura: 'Coordinador',
  },{
    rut: '14.278.026-7',
    nombres: 'Gabriela Beatriz',
    apaterno: 'Muñoz',
    amaterno: 'Ortiz',
    funcion: 'Coordinador',
    asignatura: 'Coordinador',
  }])
  .then(respondWithResult(res, 201))
  .catch(handleError(res));
}
