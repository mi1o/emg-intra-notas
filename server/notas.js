const { Router } = require('express');
const router = new Router();

const controller = require('./notas.controller');

//lista usuarios liceo
router.get('/', controller.lista);
router.get('/seed', controller.seed);
router.post('/', controller.crear);
router.delete('/:rut', controller.borrar);
router.put('/:rut', controller.editar);

module.exports = router;
