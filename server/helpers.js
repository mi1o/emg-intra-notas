'use strict';


module.exports.respondWithResult = (res, statusCode = 200) => entity => {
  if (entity) {
    res.status(statusCode).json(entity);
  }
};

module.exports.saveUpdates = updates => entity => {
  const updated = _.merge(entity, updates);
  return updated.saveAsync()
    .then(updated => {
      return updated;
    });
};

module.exports.removeEntity = res => entity => {
  if (entity) {
    return entity.removeAsync()
      .then(() => {
        res.status(204).end();
      });
  }
};

module.exports.handleEntityNotFound = res => entity => {
  if (!entity) {
    res.status(404).end();
    return null;
  }
  return entity;
};

module.exports.handleError = (res, statusCode = 500) => err => res.status(statusCode).json(err);
