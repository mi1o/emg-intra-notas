
const Notas = require('./notas.model');
const { respondWithResult,
        saveUpdates,
        removeEntity,
        handleEntityNotFound,
        handleError } = require('./helpers');

const _ = require('lodash')

module.exports.lista = (req, res) => Notas.findAsync()
  .then(respondWithResult(res))
  .catch(handleError(res));

module.exports.crear = (req, res) => {
  Notas.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
};

module.exports.borrar = (req, res) => Notas.findOneAsync({ rut: req.params.rut })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));

module.exports.editar = (req, res) => {
  Notas.findByIdAsync(req.body._id)
    .then(handleEntityNotFound(res))
    .then(usuario => {

      const nUsuario = _.merge(usuario, req.body);

      return nUsuario.saveAsync();
    })
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
};

module.exports.seed = (req, res) => {
  Notas.createAsync([{
    rut: '16.601.929-3',
    nombres: 'Camilo Nicolás',
    apaterno: 'Placencia',
    amaterno: 'Ulloa',
    autorizacion: '1234123',
    anio: 2016,
    periodo: 3,
    nivel: 'Segundo Nivel Educación Media',
    lenguaje: 1,
    matematicas: 1,
    sociales: 1,
    cienciasNaturales: 7,
    idiomaExtranjero: 7,
    laboral: 7,
    promedio: 7,
    situacionFinal: 'Aprobado',
    fechaExamen: '06/06/2016'
  }])
  .then(respondWithResult(res, 201))
  .catch(handleError(res));
}
