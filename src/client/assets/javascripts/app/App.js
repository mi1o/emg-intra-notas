import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import NavBar from '../components/NavBar';
import HomeNav from '../components/HomeNav';

const App = (props) => (
  <div className="emg-app">
    <NavBar />
    <div className="container">
      <HomeNav location={props.location} />
      {React.cloneElement({...props}.children, {...props})}
    </div>
  </div>
);

App.propTypes = {
  children: PropTypes.element.isRequired
};

export default App;
