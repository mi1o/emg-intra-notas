import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';

import App from './App';
import UsuariosView from 'features/usuarios';
import NotasView from 'features/notas';
import NotFoundView from 'components/NotFound';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={NotasView} />
    <Route path="usuarios" component={UsuariosView} />
    <Route path="404" component={NotFoundView} />
    <Redirect from="*" to="404" />
  </Route>
);
