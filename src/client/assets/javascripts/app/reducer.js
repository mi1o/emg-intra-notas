import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import usuarios from 'features/usuarios/usuariosReducer';
import notas from 'features/notas/notas.reducer';

export default combineReducers({
  routing,
  usuarios,
  notas
});
