import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const isActiveLi = (activePath,liPath) => activePath === liPath ? "active" : null;

const HomeNav = (props) => (
  <ul className="nav nav-tabs">
    <li role="presentation" className={isActiveLi(props.location.pathname, '/')}>
      <Link to="/">Notas y Actas</Link>
    </li>
    <li role="presentation" className={isActiveLi(props.location.pathname, '/usuarios')}>
      <Link to="/usuarios">Lista Profesores</Link>
    </li>
  </ul>
);

HomeNav.propTypes = {
  location: PropTypes.object.isRequired
};

export default HomeNav;
