import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const NavBar = (props) => (
  <nav className="navbar navbar-default navbar-static-top">
    <div className="container">
      <div className="navbar-header">
        <Link className="navbar-brand" to="/">EMG INTRANET</Link>
      </div>
    </div>
  </nav>
);

NavBar.propTypes = {
  //children: PropTypes.element.isRequired
};

export default NavBar;
