import React, { Component, PropTypes } from 'react';

const ListaFiltrosNotas = ({ filtro, onChangeFiltro }) => {
  return (
    <div className="form-inline">
      <div className="form-group">
        <label htmlFor="anioNotas">A&ntilde;o</label>
        <select
          className="emg-input--enlinea"
          id="anioNotas"
          value={filtro.anio}
          onChange={onChangeFiltro('anio')}>
          <option value="2017">2017</option>
          <option value="2016">2016</option>
          <option value="2015">2015</option>
          <option value="2014">2014</option>
          <option value="2013">2013</option>
        </select>
      </div>
      <div className="form-group">
        <label htmlFor="periodoNotas">Per&iacute;odo</label>
        <select
          className="emg-input--enlinea"
          id="periodoNotas"
          value={filtro.periodo}
          onChange={onChangeFiltro('periodo')}>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
        </select>
      </div>
      <div className="form-group">
        <label htmlFor="nivelNotas">Nivel Educat&iacute;vo</label>
        <select
          className="emg-input--enlinea"
          id="nivelNotas"
          value={filtro.nivel}
          onChange={onChangeFiltro('nivel')}>
          <option value="primer_nivel_basica">Primer Nivel B&aacute;sica</option>
          <option value="segundo_nivel_basica">Segundo Nivel B&aacute;sica</option>
          <option value="tercer_nivel_basica">Tercer Nivel B&aacute;sica</option>
          <option value="primer_nivel_media">Primer Nivel Media</option>
          <option value="segundo_nivel_media">Segundo Nivel Media</option>
          <option value="tercer_nivel_basica_laboral">Tercer Nivel B&aacute;sica Laboral</option>
          <option value="segundo_nivel_media_laboral">Segundo Nivel Media Laboral</option>
        </select>
      </div>
    </div>
  )
};

ListaFiltrosNotas.propTypes = {
  filtro: PropTypes.object.isRequired,
  onChangeFiltro: PropTypes.func.isRequired
}


export default ListaFiltrosNotas;
