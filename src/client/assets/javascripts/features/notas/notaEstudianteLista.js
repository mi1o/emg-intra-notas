import React, { Component, PropTypes } from 'react';

const NotaEstudianteLista = ({ estudiante, onChange, onBlur, onUpdateCalcularPromedio, onUpdateCambiarStatus, onClick }) => (
  <tr>
    <td>
      <input type="text"
        value={estudiante.rut}
        data-key="rut"
        className="emg-input--enlinea-seamless"
        onChange={onChange('rut', estudiante)}
        onBlur= {onBlur(estudiante)} />
    </td>
    <td>
      <input type="text"
        value={estudiante.apaterno}
        data-key="apaterno"
        className="emg-input--enlinea-seamless"
        onChange={onChange('apaterno', estudiante)}
        onBlur= {onBlur(estudiante)}/>
    </td>
    <td>
      <input type="text"
        value={estudiante.amaterno}
        data-key="amaterno"
        className="emg-input--enlinea-seamless"
        onChange={onChange('amaterno', estudiante)}
        onBlur= {onBlur(estudiante)} />
    </td>
    <td>
      <textarea type="text" value={estudiante.nombres}
        data-key="nombres"
        className="emg-input--enlinea-seamless"
        onChange={onChange('nombres', estudiante)}
        onBlur= {onBlur(estudiante)} style={{resize: "none"}}></textarea></td>
    <td>
      <input type="text"
        value={estudiante.autorizacion}
        data-key="autorizacion"
        className="emg-input--enlinea-seamless"
        onChange={onChange('autorizacion', estudiante)}
        onBlur= {onBlur(estudiante)} />
    </td>
    <td>
      <input type="number"
        value={estudiante.lenguaje}
        data-key="lenguaje"
        className="emg-input--enlinea-seamless emg-input--centrado"
        onChange={onChange('lenguaje', estudiante)}
        onBlur= {onBlur(estudiante)} /></td>
    <td>
      <input type="number"
        value={estudiante.matematicas}
        data-key="matematicas"
        className="emg-input--enlinea-seamless emg-input--centrado"
        onChange={onChange('matematicas', estudiante)}
        onBlur= {onBlur(estudiante)} /></td>
    <td>
      <input type="number" value={estudiante.sociales}
        data-key="sociales"
        className="emg-input--enlinea-seamless emg-input--centrado"
        onChange={onChange('sociales', estudiante)}
        onBlur= {onBlur(estudiante)} /></td>
    <td>
      <input type="number" value={estudiante.cienciasNaturales}
        data-key="cienciasNaturales"
        className="emg-input--enlinea-seamless emg-input--centrado"
        onChange={onChange('cienciasNaturales', estudiante)}
        onBlur= {onBlur(estudiante)} /></td>
    <td>
      <input type="number" value={estudiante.idiomaExtranjero}
        data-key="idiomaExtranjero"
        className="emg-input--enlinea-seamless emg-input--centrado"
        onChange={onChange('idiomaExtranjero', estudiante)}
        onBlur= {onBlur(estudiante)} /></td>
    <td>
      <input type="number" value={estudiante.laboral}
        data-key="laboral"
        className="emg-input--enlinea-seamless emg-input--centrado"
        onChange={onChange('laboral', estudiante)}
        onBlur= {onBlur(estudiante)} /></td>
    <td>
      <input type="number"
        data-key="promedio"
        value={onUpdateCalcularPromedio(estudiante)}
        className="emg-input--enlinea-seamless emg-input--centrado" /></td>
    <td>
      <input type="text"
        data-key="situacionFinal"
        value={onUpdateCambiarStatus(estudiante)}
        className="emg-input--enlinea-seamless" /></td>
    <td>
      <input type="date" value={estudiante.fechaExamen}
        data-key="fechaExamen"
        className="emg-input--enlinea-seamless"
        onChange={onChange('fechaExamen', estudiante)}
        onBlur= {onBlur(estudiante)} /></td>
    <td><button className="btn btn-sm btn-danger" onClick={onClick(estudiante)}><i className="fa fa-trash-o" aria-hidden="true"></i></button></td>
  </tr>
);

export default NotaEstudianteLista;
