'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

const HeaderTablaNotas = props => (
  <thead>
    <tr>
      <th>RUT</th>
      <th>Ap. Paterno</th>
      <th>Ap. Materno</th>
      <th>Nombres</th>
      <th>Autorizaci&oacute;n</th>
      <th>Lenguaje y Com.</th>
      <th>Matem&aacute;ticas</th>
      <th>Sociales</th>
      <th>Ciencias Naturales</th>
      <th>Idioma Extranjero</th>
      <th>Laboral</th>
      <th>Promedio Final</th>
      <th>Situaci&oacute;n Final</th>
      <th>Examen Final</th>
      <th></th>
    </tr>
  </thead>
);

HeaderTablaNotas.propTypes = {

};

export default HeaderTablaNotas;
