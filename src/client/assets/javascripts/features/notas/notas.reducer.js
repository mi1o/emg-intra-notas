import {
  NOTAS_INICIO,
  NOTAS_EXITO,
  NOTAS_FALLO,
  CHANGE_FILTRO,
  BORRAR_NOTAS_INICIO,
  BORRAR_NOTAS_EXITO,
  BORRAR_NOTAS_FALLO,
  CREAR_NOTAS_INICIO,
  CREAR_NOTAS_EXITO,
  CREAR_NOTAS_FALLO,
  PRE_EDITAR_NOTAS,
  EDITAR_NOTAS_INICIO,
  EDITAR_NOTAS_EXITO,
  EDITAR_NOTAS_FALLO,
  NOTAS_FORM_INICIAR,
  NOTAS_FORM_ACTUALIZAR,
  NOTAS_FORM_BORRAR
} from './notas.actions';

import _ from 'lodash';

const INITIAL_STATE = {
  notas: [],
  notasCargando: false,
  notasCargadas: false,
  notasBorrando: false,
  notasCreando: false,
  notasEditando: false,
  error: null,
  filtro: {
    anio: 2016,
    periodo: 3,
    nivel: "segundo_nivel_media"
  }
};

const NotasReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NOTAS_INICIO:
      return Object.assign({}, state, { notasCargando: true });
    case NOTAS_EXITO:
      return Object.assign({}, state, {
        notasCargando: false,
        notasCargadas: true,
        notas: action.payload.notas
      });
    case NOTAS_FALLO:
      return Object.assign({}, state, { notasCargando: false, notasCargadas: false });

    case CREAR_NOTAS_INICIO:
      return Object.assign({}, state, { notasCreando: true });
    case CREAR_NOTAS_EXITO: {
      const notas = [...state.notas, action.payload.nota];

      return Object.assign({}, state, { notasCreando: false, notas });
    }
    case CREAR_NOTAS_FALLO:
      return Object.assign({}, state, { notasCreando: false, error: action.payload.error });


    case BORRAR_NOTAS_INICIO:
      return Object.assign({}, state, {
        notasBorrando: true
      });
    case BORRAR_NOTAS_EXITO: {
      const indexNota = state.notas.findIndex(nota => nota._id === action.payload.nota._id);
      const notas = [ ...state.notas.slice(0, indexNota), ...state.notas.slice(+indexNota +1)];
      return Object.assign({}, state, {
        notasBorrando: false,
        notas
      });
    }
    case BORRAR_NOTAS_FALLO:
      return Object.assign({}, state, {
        notasBorrando: false,
        error: action.payload.error
      });

    case CHANGE_FILTRO:
      return Object.assign({}, state, {
        filtro: { ...state.filtro, [action.payload.filtro]: action.payload.value }
      });

    case NOTAS_FORM_INICIAR:
      return Object.assign({}, state, {
        notasForm: {
          rut: '',
          nombres: '',
          apaterno: '',
          amaterno: '',
          autorizacion: '',
          anio: state.filtro.anio,
          periodo: state.filtro.periodo,
          nivel: state.filtro.nivel,
          lenguaje: '',
          matematicas: '',
          sociales: '',
          cienciasNaturales: '',
          idiomaExtranjero: '',
          laboral: '',
          promedio: '',
          situacionFinal: '',
          fechaExamen: ''
        }
      });
    case NOTAS_FORM_ACTUALIZAR: {
      return Object.assign({}, state, {
        notasForm: {
          ...state.notasForm,
          [action.payload.value]: action.payload.data
        }
      });
    }

    case EDITAR_NOTAS_INICIO:
      return { ...state, notasEditando: true };

    case PRE_EDITAR_NOTAS: {
      const tNota = action.payload.nota;
      const indexNota = state.notas.findIndex(nota => nota._id === tNota._id);
      const notas = [...state.notas.slice(0, indexNota), tNota, ...state.notas.slice(+indexNota +1)];

      return { ...state, notas };
    }
    case EDITAR_NOTAS_EXITO: {
      const tNota = action.payload.nota;
      const indexNota = state.notas.findIndex(nota => nota._id === tNota._id);
      const notas = [...state.notas.slice(0, indexNota), tNota, ...state.notas.slice(+indexNota +1)];

      return { ...state, notas, notasEditando: false };
    }
    case EDITAR_NOTAS_FALLO:
      return { ...state, notasEditando: false, error: action.payload.error };

    case NOTAS_FORM_BORRAR:
      return Object.assign({}, state, { notasForm: {} });

    default:
      return state;
  }
}

export default NotasReducer;
