
//import _ from 'lodash';
import axios from 'axios';

export const NOTAS_INICIO = 'NOTAS_INICIO';
export const NOTAS_EXITO = 'NOTAS_EXITO';
export const NOTAS_FALLO = 'NOTAS_FALLO';

const notasInicio = () => ({
  type: NOTAS_INICIO
});

const notasExito = notas => ({
  type: NOTAS_EXITO,
  payload: {
    notas
  }
});

const notasFallo = () => ({
  type: NOTAS_FALLO
});

export const PedirNotas = () => dispatch => {
  dispatch(notasInicio());
  axios.get('/api/notas/')
    .then(({ data: notas }) => dispatch(notasExito(notas)))
    .catch(error => dispatch(notasFallo(error)))
}

export const EDITAR_NOTAS_INICIO = 'EDITAR_NOTAS_INICIO';
export const EDITAR_NOTAS_EXITO = 'EDITAR_NOTAS_EXITO';
export const PRE_EDITAR_NOTAS = 'PRE_EDITAR_NOTAS';
export const EDITAR_NOTAS_FALLO = 'EDITAR_NOTAS_FALLO';

const editarNotasInicio = () => ({
  type: EDITAR_NOTAS_INICIO
});

const editarNotasExito = nota => ({
  type: EDITAR_NOTAS_EXITO,
  payload: {
    nota
  }
});

const preEditarNotas = nota => ({
  type: PRE_EDITAR_NOTAS,
  payload: {
    nota
  }
})

const editarNotasFallo = error => ({
  type: EDITAR_NOTAS_FALLO,
  payload: {
    error
  }
});

export const EditarNotas = nota => (dispatch, getState) => {
  const { notas } = getState();

  if(notas.notasEditando)
    return false;

  dispatch(editarNotasInicio());

  axios.put(`/api/notas/${nota.rut}`, nota)
    .then(({ data: notas }) => dispatch(editarNotasExito(nota)))
    .catch(error => dispatch(editarNotasFallo(error)))
};

export const PreEditarNotas = nota => (dispatch) => dispatch(preEditarNotas(nota));



export const CREAR_NOTAS_INICIO = 'CREAR_NOTAS_INICIO';
export const CREAR_NOTAS_EXITO = 'CREAR_NOTAS_EXITO';
export const CREAR_NOTAS_FALLO = 'CREAR_NOTAS_FALLO';

const crearNotasInicio = () => ({
  type: CREAR_NOTAS_INICIO
});

const crearNotasExito = nota => ({
  type: CREAR_NOTAS_EXITO,
  payload: {
    nota
  }
});

const crearNotasFallo = error => ({
  type: CREAR_NOTAS_FALLO,
  payload: {
    error
  }
});

export const CrearNotas = nota => (dispatch, getState) => {
  const { notas } = getState();
  if(notas.notasCreando)
    return false;

  dispatch(crearNotasInicio());

  axios.post(`/api/notas`, nota)
    .then(({ data: notas }) => dispatch(crearNotasExito(nota)))
    .catch(error => {console.log(error); return dispatch(crearNotasFallo(error))})
}

export const BORRAR_NOTAS_INICIO = 'BORRAR_NOTAS_INICIO';
export const BORRAR_NOTAS_EXITO = 'BORRAR_NOTAS_EXITO';
export const BORRAR_NOTAS_FALLO = 'BORRAR_NOTAS_FALLO';

const borrarNotasInicio = () => ({
  type: BORRAR_NOTAS_INICIO
});

const borrarNotasExito = nota => ({
  type: BORRAR_NOTAS_EXITO,
  payload: {
    nota
  }
});

const borrarNotasFallo = () => ({
  type: BORRAR_NOTAS_FALLO
});

export const BorrarNotas = nota => (dispatch, getState) => {
  const { notas } = getState();

  if(notas.notasBorrando)
    return false;

  dispatch(borrarNotasInicio());
  axios.delete(`/api/notas/${nota.rut}`)
    .then(({ data: notas }) => dispatch(borrarNotasExito(nota)))
    .catch(error => dispatch(borrarNotasFallo(error)))
}


export const CHANGE_FILTRO = 'CHANGE_FILTRO';

const cambiarFiltro = (value, filtro) => ({
  type: CHANGE_FILTRO,
  payload: {
    value,
    filtro
  }
})

export const CambiarFiltro = (value, filtro) => dispatch => dispatch(cambiarFiltro(value, filtro));

export const NOTAS_FORM_INICIAR = 'NOTAS_FORM_INICIAR';
export const NOTAS_FORM_ACTUALIZAR = 'NOTAS_FORM_ACTUALIZAR';
export const NOTAS_FORM_BORRAR = 'NOTAS_FORM_BORRAR';


const notasFormIniciar = () => {
  return {
    type: NOTAS_FORM_INICIAR
  };
}

const notasFormActualizar = (value, data) => {
  return {
    type: NOTAS_FORM_ACTUALIZAR,
    payload: {
      value,
      data
    }
  };
}

const notasFormBorrar = () => {
  return {
    type: NOTAS_FORM_BORRAR
  }
}

export function NotasFormIniciar() {
  return dispatch => dispatch(notasFormIniciar());
}

export function NotasForm(value, data) {
  return dispatch => dispatch(notasFormActualizar(value,data));
}

export function NotasFormBorrar() {
  return dispatch => dispatch(notasFormBorrar());
}
