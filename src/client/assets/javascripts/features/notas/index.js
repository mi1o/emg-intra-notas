'use strict';

import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import * as actions from './notas.actions';

import HeaderTablaNotas from './headerTablaNotas';
import ListaFiltrosNotas from './listaFiltrosNotas';
import FormNuevaNota from './formNuevaNota';
import NotaEstudianteLista from './notaEstudianteLista';

import './styles.scss';
import _ from 'lodash';

class NotasPage extends Component {
  constructor(){
    super();
    this.renderEstudiante = this.renderEstudiante.bind(this);
    this.onChangeFiltro = this.onChangeFiltro.bind(this);
    this.onClickBorrarNota = this.onClickBorrarNota.bind(this);

    this.onClickCrearNota = this.onClickCrearNota.bind(this);
    this.onChangeUpdateForm = this.onChangeUpdateForm.bind(this);
    this.onChangeUpdateNota = this.onChangeUpdateNota.bind(this);
    this.onBlurUpdateNota = this.onBlurUpdateNota.bind(this);
    this.onUpdateCalcularPromedio = this.onUpdateCalcularPromedio.bind(this);
    this.onUpdateCambiarStatus = this.onUpdateCambiarStatus.bind(this);

    this.formatRut = this.formatRut.bind(this);
  }

  componentWillMount() {
    this.props.PedirNotas();
    this.props.NotasFormIniciar();
  }

  renderEstudiante(notas, filtro, onClick) {
    if(!notas)
      return null;

    const notasFiltradas = notas
      .filter(estudiante => (estudiante.periodo === +filtro.periodo && estudiante.anio === +filtro.anio && estudiante.nivel === filtro.nivel));

    return notasFiltradas
      .map(estudiante => (
        <NotaEstudianteLista
          key={estudiante._id}
          estudiante={estudiante}
          onChange={this.onChangeUpdateNota}
          onBlur={this.onBlurUpdateNota}
          onClick={onClick}
          onUpdateCalcularPromedio={this.onUpdateCalcularPromedio}
          onUpdateCambiarStatus={this.onUpdateCambiarStatus}
        />
      ));
  }

  onClickBorrarNota(nota) {
    return e => {
      e.preventDefault();
      this.props.BorrarNotas(nota);
    }
  }

  onClickCrearNota(nota) {
    return e => {
      e.preventDefault();

      this.props.CrearNotas(nota);
      this.props.NotasFormIniciar();
    }
  }

  onChangeUpdateNota(campo, estudianteNota) {
    return e => {
      e.preventDefault;
      const value = e.target.value;
      const isAsignatura = (campo === 'lenguaje' || campo === 'matematicas' || campo === 'sociales' || campo === 'cienciasNaturales' || campo === 'idiomaExtranjero' ||  campo === 'laboral');
      const nota = isAsignatura ? _.round(value, 1) : value;

      if(((isAsignatura && nota > 7) ||  (isAsignatura && nota < 1 )) && value !== '') {
        return false;
      }

      const preEstudiante = { ...estudianteNota, [campo]: nota };
      const promedio = this.onUpdateCalcularPromedio(preEstudiante);
      const newEstudiante = { ...preEstudiante, promedio };
      const situacionFinal = this.onUpdateCambiarStatus(newEstudiante);

      this.props.PreEditarNotas({ ...newEstudiante, situacionFinal });
    }
  }

  onBlurUpdateNota(estudianteNota) {
    return e => {
      e.preventDefault();
      const rut = (e.target.dataset.key === 'rut') ? this.formatRut(estudianteNota.rut) : estudianteNota.rut;
      this.props.EditarNotas({ ...estudianteNota, rut });
    }
  }

  onChangeUpdateForm(value) {
    return e => {
      e.preventDefault();

      const data = e.target.value;
      this.props.NotasForm(value, data);
    }
  }

  onChangeFiltro(filtro) {
    return e => {
      const value = e.target.value.trim();
      this.props.CambiarFiltro(value,filtro);
      this.props.NotasForm(filtro, value);
    }
  }

  onUpdateCalcularPromedio(estudiante) {
    const lenguaje    = _.round(estudiante.lenguaje,1);
    const matematicas = _.round(estudiante.matematicas,1);
    const sociales    = _.round(estudiante.sociales,1);
    const csNaturales = _.round(estudiante.cienciasNaturales,1);
    const extranjero  = _.round(estudiante.idiomaExtranjero,1);
    const laboral     = _.round(estudiante.laboral,1);

    let promedio;

    switch (estudiante.nivel) {
      case 'primer_nivel_basica':
        promedio = _.round((lenguaje + matematicas) / 2, 1);
      break;

      case 'segundo_nivel_basica':
      case 'tercer_nivel_basica':
        promedio = _.round((lenguaje + matematicas + sociales + csNaturales) / 4, 1);
      break;

      case 'primer_nivel_media':
      case 'segundo_nivel_media':
        promedio = _.round((lenguaje + matematicas + sociales + csNaturales + extranjero) / 5, 1);
      break;

      case 'tercer_nivel_basica_laboral':
      case 'segundo_nivel_media_laboral':
        promedio = _.round(laboral, 1);
      break;

      default:
        promedio = _.round((lenguaje + matematicas) / 2, 1);
      break;
    }

    return promedio;
  }

  onUpdateCambiarStatus(estudiante) {
    const { nivel, promedio, lenguaje, matematicas, sociales,
      cienciasNaturales: csNaturales, idiomaExtranjero: extranjero, laboral } = estudiante;

    if(promedio >= 4) {
      switch (nivel) {
        case 'primer_nivel_basica':
          return (promedio >=4 && lenguaje >= 4 && matematicas >= 4) ? 'Aprobado' : 'Reprobado';
        case 'segundo_nivel_basica':
        case 'tercer_nivel_basica':
          return (promedio >=4 && lenguaje >= 4 && matematicas >= 4 && csNaturales >= 4 && sociales >= 4) ? 'Aprobado' : 'Reprobado';
        case 'primer_nivel_media':
        case 'segundo_nivel_media':
          return (promedio >=4 && lenguaje >= 4 && matematicas >= 4 && csNaturales >= 4 && sociales >= 4 && extranjero >= 4) ? 'Aprobado' : 'Reprobado';
        case 'tercer_nivel_basica_laboral':
        case 'segundo_nivel_media_laboral':
          return (promedio >= 4 && laboral >= 4) ? 'Aprobado' : 'Reprobado';
        default:
          return (promedio >= 4) ? 'Aprobado' : 'Reprobado';
      }
    }
    else {
      return 'Reprobado';
    }
  }

  formatRut(rut) {
    const regex = /\d{1,2}\.\d{3}\.\d{3}-[0-9k]{1}/ig;

    if(regex.test(rut))
      return rut;

    let newRut = rut.replace(/[\.-]/ig,'');
    let digito = newRut.slice(-1);
    let ultSeg = newRut.slice(-4,-1);
    let medSeg = newRut.slice(-7,-4);
    let primDi = newRut.slice(-11,-7);

    return primDi + "." + medSeg + "." + ultSeg + "-" + digito;
  }

  render() {
    const { notas } = this.props;

    return (
      <div className="emg-notas">
        <div className="page-header">
          <h1>Sistema de Notas</h1>
        </div>

        <ListaFiltrosNotas filtro={notas.filtro} onChangeFiltro={this.onChangeFiltro}/>

        <div className="table-responsive table-bordered">
          <table className="table">
            <HeaderTablaNotas />
            <tbody>
              {this.renderEstudiante(notas.notas, notas.filtro, this.onClickBorrarNota)}
            </tbody>
            <FormNuevaNota
              notasForm={notas.notasForm}
              onClickCrearNota={this.onClickCrearNota}
              onChangeUpdateForm={this.onChangeUpdateForm} />
          </table>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    notas: state.notas
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    PedirNotas: () => dispatch(actions.PedirNotas()),
    CambiarFiltro: (value, filtro) => dispatch(actions.CambiarFiltro(value, filtro)),
    BorrarNotas: nota => dispatch(actions.BorrarNotas(nota)),
    CrearNotas: nota => dispatch(actions.CrearNotas(nota)),
    EditarNotas: nota => dispatch(actions.EditarNotas(nota)),
    PreEditarNotas: nota => dispatch(actions.PreEditarNotas(nota)),

    NotasFormIniciar: () => dispatch(actions.NotasFormIniciar()),
    NotasForm: (value, data) => dispatch(actions.NotasForm(value, data)),
    NotasFormBorrar: () => dispatch(actions.NotasFormBorrar())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NotasPage)
