import React, { Component, PropTypes } from 'react';

const FormNuevaNota = ({ notasForm, onClickCrearNota, onChangeUpdateForm }) => {
  return !!notasForm ? (
    <tfoot>
      <tr>
        <td>
          <input className="emg-input--enlinea"
            placeholder="RUT Estudiante"
            value={notasForm.rut}
            onChange={onChangeUpdateForm('rut')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Apellido Paterno"
            value={notasForm.apaterno}
            onChange={onChangeUpdateForm('apaterno')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Apellido Materno"
            value={notasForm.amaterno}
            onChange={onChangeUpdateForm('amaterno')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Nombres"
            value={notasForm.nombres}
            onChange={onChangeUpdateForm('nombres')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Autorizaci&oacute;n"
            value={notasForm.autorizacion}
            onChange={onChangeUpdateForm('autorizacion')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Nota Lenguaje"
            value={notasForm.lenguaje}
            onChange={onChangeUpdateForm('lenguaje')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Nota Matem&aacute;ticas"
            value={notasForm.matematicas}
            onChange={onChangeUpdateForm('matematicas')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Nota Sociales"
            value={notasForm.sociales}
            onChange={onChangeUpdateForm('sociales')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Nota Ciencias"
            value={notasForm.cienciasNaturales}
            onChange={onChangeUpdateForm('cienciasNaturales')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Nota Idioma Ex."
            value={notasForm.idiomaExtranjero}
            onChange={onChangeUpdateForm('idiomaExtranjero')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Laboral"
            value={notasForm.laboral}
            onChange={onChangeUpdateForm('laboral')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Promedio"
            value={notasForm.promedio}
            onChange={onChangeUpdateForm('promedio')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Situaci&oacute;n Final"
            value={notasForm.situacionFinal}
            onChange={onChangeUpdateForm('situacionFinal')}/>
        </td>
        <td>
          <input className="emg-input--enlinea"
            placeholder="Fecha Ex&aacute;men"
            value={notasForm.fechaExamen}
            onChange={onChangeUpdateForm('fechaExamen')}/>
        </td>
        <td>
          <button className="btn btn-sm btn-success" onClick={onClickCrearNota(notasForm)}><i className="fa fa-plus" aria-hidden="true"></i></button></td>
      </tr>
    </tfoot>
  ) : null;
};

FormNuevaNota.propTypes = {

};

export default FormNuevaNota;
