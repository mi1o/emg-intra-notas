import React, { Component, PropTypes } from 'react';

const ListaUsuarios = ({ usuario, borrarUsuario }) => {
  return !!usuario ? (
    <tr>
      <td>{usuario.rut}</td>
      <td>{usuario.apaterno}</td>
      <td>{usuario.amaterno}</td>
      <td>{usuario.nombres}</td>
      <td>{usuario.funcion}</td>
      <td>{usuario.asignatura}</td>
      <td>
        <button className="btn btn-danger btn-sm" onClick={borrarUsuario(usuario)}><i className="fa fa-trash-o" aria-hidden="true"></i></button>
      </td>
    </tr>
  ) : null;
};

ListaUsuarios.propTypes = {
  usuario: PropTypes.object.isRequired,
  borrarUsuario: PropTypes.func.isRequired
}

export default ListaUsuarios;
