
//import _ from 'lodash';
import axios from 'axios';

export const USUARIOS_INICIO = 'USUARIOS_INICIO';
export const USUARIOS_EXITO = 'USUARIOS_EXITO';
export const USUARIOS_FALLO = 'USUARIOS_FALLO';

function usuariosInicio() {
  return {
    type: USUARIOS_INICIO
  }
}

function usuariosExito(usuarios) {
  return {
    type: USUARIOS_EXITO,
    payload: {
      usuarios
    }
  }
}

function usuariosFallo() {
  return {
    type: USUARIOS_FALLO
  }
}

export function PedirUsuarios() {
  return dispatch => {
    dispatch(usuariosInicio());
    axios.get('/api/usuarios/')
      .then(({ data: usuarios }) => dispatch(usuariosExito(usuarios)))
      .catch(error => dispatch(usuariosFallo(error)))
  }
}

export const USUARIO_BORRAR_INICIO = 'USUARIO_BORRAR_INICIO';
export const USUARIO_BORRAR_EXITO = 'USUARIO_BORRAR_EXITO';
export const USUARIO_BORRAR_FALLO = 'USUARIO_BORRAR_FALLO';

function borrarUsuarioInicio() {
  return {
    type: USUARIO_BORRAR_INICIO
  }
}

function borrarUsuarioExito(usuario) {
  return {
    type: USUARIO_BORRAR_EXITO,
    payload: {
      usuario
    }
  }
}

function borrarUsuarioFallo() {
  return {
    type: USUARIO_BORRAR_FALLO
  }
}

export function BorrarUsuario(usuario) {
  return dispatch => {
    dispatch(borrarUsuarioInicio());

    axios.delete(`/api/usuarios/${usuario.rut}`)
      .then(({ data: usuarios }) => dispatch(borrarUsuarioExito(usuario)))
      .catch(error => dispatch(borrarUsuarioFallo(error)));
  };
}

export const USUARIO_FORM_INICIAR = 'USUARIO_FORM_INICIAR';
export const USUARIO_FORM_ACTUALIZAR = 'USUARIO_FORM_ACTUALIZAR';
export const USUARIO_FORM_BORRAR = 'USUARIO_FORM_BORRAR';

function usuarioFormIniciar() {
  return {
    type: USUARIO_FORM_INICIAR
  };
}

function usuarioFormActualizar(value, data) {
  return {
    type: USUARIO_FORM_ACTUALIZAR,
    payload: {
      value,
      data
    }
  };
}

function usuarioFormBorrar() {
  return {
    type: USUARIO_FORM_BORRAR
  }
}

export function UsuarioFormIniciar() {
  return dispatch => dispatch(usuarioFormIniciar());
}

export function UsuarioForm(value, data) {
  return dispatch => dispatch(usuarioFormActualizar(value,data));
}

export function UsuarioFormBorrar() {
  return dispatch => dispatch(usuarioFormBorrar());
}


export const USUARIO_CREAR_INICIO = 'USUARIO_CREAR_INICIO';
export const USUARIO_CREAR_EXITO = 'USUARIO_CREAR_EXITO';
export const USUARIO_CREAR_FALLO = 'USUARIO_CREAR_FALLO';

function usuarioCrearInicio() {
  return {
    type: USUARIO_CREAR_INICIO
  }
}

function usuarioCrearExito(usuario) {
  return {
    type: USUARIO_CREAR_EXITO,
    payload: {
      usuario
    }
  }
}

function usuarioCrearFallo() {
  return {
    type: USUARIO_CREAR_FALLO
  }
}

export function UsuarioCrear(usuario, cb) {
  return dispatch => {
    dispatch(usuarioCrearInicio());

    axios.post('/api/usuarios/', usuario)
      .then(({ data: usuarioNuevo }) => dispatch(usuarioCrearExito(usuarioNuevo)))
      .catch(error => dispatch(usuarioCrearFallo(error)));
  }
}
