import React, { Component, PropTypes } from 'react';

const HeaderTabla = props => (
  <thead>
    <tr>
      <th>RUT</th>
      <th>Ap. Paterno</th>
      <th>Ap. Materno</th>
      <th>Nombres</th>
      <th>Función</th>
      <th>Asignatura</th>
      <th>Acciones</th>
    </tr>
  </thead>
);

HeaderTabla.propTypes = {};


export default HeaderTabla;
