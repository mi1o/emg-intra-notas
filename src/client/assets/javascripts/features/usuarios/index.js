'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import './styles.scss';

import HeaderTabla from './headerTabla';
import ListaUsuarios from './listaUsuarios';
import NuevoUsuarioForm from './nuevoUsuarioForm';

import * as actions from './usuariosActions';

class UsuariosPage extends Component {
  constructor(){
    super();

    this.getListaFunciones = this.getListaFunciones.bind(this);
    this.getListaAsignaturas = this.getListaAsignaturas.bind(this);
    this.borrarUsuario = this.borrarUsuario.bind(this);
    this.usuarioFormIniciar = this.usuarioFormIniciar.bind(this);
    this.usuarioFormEditar = this.usuarioFormEditar.bind(this);
    this.usuarioFormBorrar = this.usuarioFormBorrar.bind(this);

    this.usuarioCrear = this.usuarioCrear.bind(this);
  }

  componentWillMount() {
    this.props.PedirUsuarios();
  }
  componentDidMount() {
    this.props.UsuarioFormIniciar();
  }

  borrarUsuario(usuario) {
    return e => {
      e.preventDefault();
      this.props.BorrarUsuario(usuario);
    }
  }

  usuarioFormIniciar() {
    this.props.UsuarioFormIniciar();
  }

  usuarioFormEditar(value) {
    return e => {
      e.preventDefault();
      this.props.UsuarioForm(value, e.target.value);
    }
  }

  usuarioFormBorrar() {
    this.props.UsuarioFormBorrar();
    this.props.UsuarioFormIniciar();
  }

  usuarioCrear(e) {
    e.preventDefault();
    this.props.UsuarioCrear(this.props.usuarios.usuarioForm, this.usuarioFormBorrar);
    this.usuarioFormBorrar();
  }

  getListaFunciones(usuarioFormEditar, value) {
    return (
      <select className="emg-input--enlinea" onChange={usuarioFormEditar('funcion')} value={value
      }>
        <option value="-1">Seleccionar Función</option>
        <option value="coordinador">Coordinador Media</option>
        <option value="docente">Docente Examinador</option>
        <option value="director">Director</option>
      </select>
    )
  }

  getListaAsignaturas(usuarioFormEditar, value) {
    return (
      <select className="emg-input--enlinea" onChange={usuarioFormEditar('asignatura')} value={value}>
        <option value="-1">Seleccionar Asignatura</option>
        <option value="lenguaje">Lenguaje</option>
        <option value="matematicas">Matemáticas</option>
        <option value="sociales">Sociales</option>
        <option value="naturales">Naturales</option>
        <option value="ingles">Inglés</option>
        <option value="filosofia">Filosofía</option>
        <option value="laboral">Laboral</option>
        <option value="coordinador">Coordinador</option>
        <option value="director">Director</option>
      </select>
    )
  }

  getListaUsuarios(usuarios) {
    return !usuarios ? null
      : usuarios.map(usuario => <ListaUsuarios
        usuario={usuario}
        borrarUsuario={this.borrarUsuario}
        key={usuario._id}/>
      );
  }

  render() {

    return (
      <div className="emg-usuarios">
        <div className="page-header">
          <h1>Administraci&oacute;n Usuarios</h1>
          <div className="table-responsive">
            <table className="table">
              <HeaderTabla />
              <tbody>
                {this.getListaUsuarios(this.props.usuarios.usuariosLista)}
                <NuevoUsuarioForm
                  getListaFunciones={this.getListaFunciones}
                  getListaAsignaturas={this.getListaAsignaturas}
                  usuarioFormEditar={this.usuarioFormEditar}
                  usuarioCrear={this.usuarioCrear}
                  usuarioForm={this.props.usuarios.usuarioForm} />
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    usuarios: state.usuarios
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    PedirUsuarios: () => dispatch(actions.PedirUsuarios()),
    BorrarUsuario: usuario => dispatch(actions.BorrarUsuario(usuario)),
    UsuarioFormIniciar: () => dispatch(actions.UsuarioFormIniciar()),
    UsuarioForm: (value, data) => dispatch(actions.UsuarioForm(value, data)),
    UsuarioFormBorrar: () => dispatch(actions.UsuarioFormBorrar()),
    UsuarioCrear: usuario => dispatch(actions.UsuarioCrear(usuario))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsuariosPage)
