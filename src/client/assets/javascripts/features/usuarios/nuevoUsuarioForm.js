
import React, { PropTypes, Component } from 'react';

const FormNuevoUsuario = ({
  getListaFunciones,
  getListaAsignaturas,
  usuarioFormEditar,
  usuarioCrear,
  usuarioForm
}) => (
  <tr>
    <td><input type="text" className="emg-input--enlinea" placeholder="RUT" onChange={usuarioFormEditar('rut')} value={usuarioForm.rut} /></td>
    <td><input type="text" className="emg-input--enlinea" placeholder="Apellido Paterno"  onChange={usuarioFormEditar('apaterno')} value={usuarioForm.apaterno} /></td>
    <td><input type="text" className="emg-input--enlinea" placeholder="Apellido Materno" onChange={usuarioFormEditar('amaterno')} value={usuarioForm.amaterno} /></td>
    <td><input type="text" className="emg-input--enlinea" placeholder="Nombres" onChange={usuarioFormEditar('nombres')} value={usuarioForm.nombres} /></td>
    <td>{getListaFunciones(usuarioFormEditar, usuarioForm.funcion)}</td>
    <td>{getListaAsignaturas(usuarioFormEditar, usuarioForm.asignatura)}</td>
    <td>
      <button className="btn btn-success btn-sm" onClick={usuarioCrear}><i className="fa fa-plus" aria-hidden="true"></i></button>
    </td>
  </tr>
);

FormNuevoUsuario.propTypes = {
  getListaFunciones: PropTypes.func.isRequired,
  getListaAsignaturas: PropTypes.func.isRequired,
  usuarioFormEditar: PropTypes.func.isRequired,
  usuarioCrear: PropTypes.func.isRequired,
  usuarioForm: PropTypes.object.isRequired
};

export default FormNuevoUsuario;
