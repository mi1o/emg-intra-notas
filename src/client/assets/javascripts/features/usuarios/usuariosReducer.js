import {
  USUARIOS_INICIO,
  USUARIOS_EXITO,
  USUARIOS_FALLO,
  USUARIO_BORRAR_INICIO,
  USUARIO_BORRAR_EXITO,
  USUARIO_BORRAR_FALLO,
  USUARIO_FORM_INICIAR,
  USUARIO_FORM_ACTUALIZAR,
  USUARIO_FORM_BORRAR,
  USUARIO_CREAR_INICIO,
  USUARIO_CREAR_EXITO,
  USUARIO_CREAR_FALLO
} from './usuariosActions';

import _ from 'lodash';

const INITIAL_STATE = {
  usuariosLista: [],
  usuariosCargados: false,
  usuariosCargando: false,
  usuarioEliminando: false,
  usuarioCreando: false,
  usuarioCreado: false,
  usuarioForm: {}
};

const UsuariosReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USUARIOS_INICIO:
      return Object.assign({}, state, { usuariosCargando: true });
    case USUARIOS_EXITO:
      return Object.assign({}, state, {
        usuariosCargando: false,
        usuariosCargados: true,
        usuariosLista: action.payload.usuarios
      });
    case USUARIOS_FALLO:
      return Object.assign({}, state, { usuariosCargando: false, usuariosCargado: false });


    case USUARIO_BORRAR_INICIO:
      return Object.assign({}, state, { usuarioEliminando: true });
    case USUARIO_BORRAR_EXITO: {
      let usuariosLista = [...state.usuariosLista];
      _.remove(usuariosLista, action.payload.usuario);

      return Object.assign({}, state, { usuarioEliminando: false, usuariosLista });
    }
    case USUARIO_BORRAR_FALLO:
      return Object.assign({}, state, { usuarioEliminando: false });


    case USUARIO_FORM_INICIAR:
      return Object.assign({}, state, { usuarioForm: {
        rut: '',
        nombres: '',
        apaterno: '',
        amaterno: '',
        funcion: '',
        asignatura: ''
      } });
    case USUARIO_FORM_ACTUALIZAR: {
      return Object.assign({}, state, {
        usuarioForm: Object.assign(
          {},
          state.usuarioForm,
          { [action.payload.value]: action.payload.data }
        )
      });
    }
    case USUARIO_FORM_BORRAR:
      return Object.assign({}, state, { usuarioForm: {} });

    case USUARIO_CREAR_INICIO:
      return Object.assign({}, state, { usuarioCreando: true, usuarioCreado: false });
    case USUARIO_CREAR_EXITO:
      return Object.assign({}, state, { usuarioCreando: false, usuarioCreado: true, usuariosLista: [...state.usuariosLista, action.payload.usuario] });
    case USUARIO_CREAR_FALLO:
      return Object.assign({}, state, { usuarioCreando: false, usuarioCreado: false });

    default:
      return state;
  }
}

export default UsuariosReducer;
