// Creates a hot reloading development environment

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const DashboardPlugin = require('webpack-dashboard/plugin');
const config = require('./config/webpack.config.development');

const app = express();
const compiler = webpack(config);

const mongoose = require('mongoose');
const bodyParser = require('body-parser');

// Apply CLI dashboard for your webpack dev server
compiler.apply(new DashboardPlugin());

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;

function log() {
  arguments[0] = '\nWebpack: ' + arguments[0];
  console.log.apply(console, arguments);
}

app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath,
  stats: {
    colors: true
  },
  historyApiFallback: true
}));

app.use(webpackHotMiddleware(compiler));


mongoose.Promise = require('bluebird');

// Connect to MongoDB
//const mongouri = 'mongodb://milo:camilo88@ds133388.mlab.com:33388/emg';
const mongouri = 'mongodb://milo:camilo88@10.10.3.20:27017/test';
const mongoopt = { db: { safe : true } };

mongoose.connect(mongouri, mongoopt);
mongoose.connection.on('error', function (err) {
  console.error(`Error de conexión con MongoDB: ${err}`);
  process.exit(-1);
});
app.use(bodyParser.json());
app.use('/api/usuarios', require('./server/usuarios'));
app.use('/api/notas', require('./server/notas'));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, './src/client/assets/index.html'));
});



app.listen(port, host, (err) => {
  if (err) {
    log(err);
    return;
  }

  log('🚧  App is listening at http://%s:%s', host, port);
});
